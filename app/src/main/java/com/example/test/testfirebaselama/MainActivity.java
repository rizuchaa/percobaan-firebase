package com.example.test.testfirebaselama;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    final Context context = this;
    private ActionMode am;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView lv = (ListView) findViewById(R.id.lv1);
        final EditText text = (EditText) findViewById(R.id.ET1);
        final Button tambah = (Button)  findViewById(R.id.Add);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1);


        lv.setAdapter(adapter);

        Firebase.setAndroidContext(this);

        new Firebase("https://testproject-eac46.firebaseio.com/Tester")
                .addChildEventListener(new ChildEventListener() {
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        adapter.add((String)dataSnapshot.child("Pesan").getValue());
                    }
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        adapter.remove((String)dataSnapshot.child("Pesan").getValue());
                    }
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) { }
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) { }
                    public void onCancelled(FirebaseError firebaseError) { }
                });

        //insert data dari smartphone
        tambah.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                new Firebase("https://testproject-eac46.firebaseio.com/Tester").push().child("Pesan")
                        .setValue(text.getText().toString());
                Toast.makeText(getApplicationContext(),"Input Data Berhasil!",Toast.LENGTH_LONG).show();
                text.setText("");
            }
        });

        //press
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int pos, long id) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setMessage("Apakah Ingin Menghapus Data?");
                alert.setTitle("Warning");

                alert.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    //delete data dari smartphone
                    public void onClick(DialogInterface arg0, int arg1) {

                        new Firebase("https://testproject-eac46.firebaseio.com/Tester").orderByChild("Pesan")
                                .equalTo((String) lv.getItemAtPosition(pos))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChildren()){
                                            DataSnapshot first = dataSnapshot.getChildren().iterator().next();
                                            first.getRef().removeValue();
                                        }
                                        Toast.makeText(getApplicationContext(),"Penghapusan Data Berhasil", Toast.LENGTH_LONG)
                                                .show();
                                    }

                                    @Override
                                    public void onCancelled(FirebaseError firebaseError) {}
                                });
                        }
                    });

                alert.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alert.create();
                alertDialog.show();

                return true;
                }
            });
    }
}
